<h1><i class="fa-solid fa-city"></i>EDITAR REVISTA</h1>
<form class="" method="post" action="<?php echo site_url('revistas/actualizarRevista'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
	<input type="hidden" name="id" id="id"
	value="<?php echo $revistaEditar->id; ?>">
  <label for="">
    <b>Nombre:</b>
  </label>
  <input type="text" name="nombre" id="nombre"
	value="<?php echo $revistaEditar->nombre; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <span id="errorNombre" class="error"></span>
  <br>
  <label for="">
    <b>Fecha:</b>
  </label>
  <input type="date" name="fecha" id="fecha"
	value="<?php echo $revistaEditar->fecha; ?>"
  placeholder="Ingrese la fecha..." class="form-control" required>
  <span id="errorFecha" class="error"></span>
  <br>

    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar </button> &nbsp &nbsp
        <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>


<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>

<script>
function validarFormulario() {
  var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var nombre = document.getElementById("nombre").value;
  var error = false;
  if (!regex.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }
  

  return !error;
}
</script>

<br>
<br>
