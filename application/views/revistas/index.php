
<h1> <i class="fa-solid fa-city"></i></i>REVISTAS</h1>

<!-- Agregar boton Hospitales -->
<div class="row">
  <div class="col-md-12 text-end">   <!--text-end-> para poner el boton a la derecha-->
    <a href="<?php echo site_url('revistas/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Revista
    </a>

    <br>
  </div>


</div>

<?php if ($listadoRevistas): ?>
  <!--Tabla Estatica-->

    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>FECHA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoRevistas as $revista): ?>
                <tr>
                  <td><?php echo $revista->id; ?></td>
                  <td><?php echo $revista->nombre; ?></td>
                  <td><?php echo $revista->fecha; ?></td>

                  <!--Boton eliminar-->
                  <td>
                    <a href="<?php echo site_url('revistas/editar/').$revista->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                      <a href="<?php echo site_url('revistas/borrar/').$revista->id; ?>" class="btn btn-danger">
                        Eliminar
                      </a>
                  </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="confirmacion-mensaje">
    <?php if ($this->session->flashdata('confirmacion')): ?>
      <div class="alert alert-success">
        <?php echo $this->session->flashdata('confirmacion'); ?>
      </div>
    <?php endif; ?>
  </div>

    <script>
            document.addEventListener('DOMContentLoaded', function () {
                var deleteButtons = document.querySelectorAll('.delete-confirm');
                deleteButtons.forEach(function (button) {
                    button.addEventListener('click', function (event) {
                        event.preventDefault();
                        var id = this.getAttribute('data-id');
                        // Mostrar un mensaje de confirmación antes de eliminar
                        iziToast.question({
                            timeout: false,
                            close: false,
                            overlay: true,
                            displayMode: 'once',
                            id: 'question',
                            zindex: 999,
                            title: 'Confirmación',
                            message: '¿Está seguro de eliminar esta revista?',
                            position: 'center',
                            buttons: [
                                ['<button><b>Sí</b></button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    // Aquí deberías hacer la llamada a tu función para eliminar el corresponsal
                                    window.location.href = '<?php echo site_url('revistas/borrar/') ?>' + id;
                                }, true],
                                ['<button>No</button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                }],
                            ],
                        });
                    });
                });

                // Mostrar mensaje de confirmación al guardar
                var confirmacionGuardado = '<?php echo $this->session->flashdata("confirmacion"); ?>';
                if (confirmacionGuardado) {
                    iziToast.success({
                        title: 'Éxito',
                        message: confirmacionGuardado,
                        position: 'topRight',
                    });
                }
            });
        </script>

<?php else: ?>

  <div class="alert alert-danger">               <!--PAra enviar mensaje de alerta-->
      No se encontraron agencias registradas
  </div>
<?php endif; ?>
