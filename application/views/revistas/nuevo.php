<h1> <i class="fa-solid fa-city"></i> Nueva Revista</h1>
<br>
<form class="" action="<?php echo site_url('revistas/guardarRevista'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
  <label for=""><b>NOMBRE</b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" value="" required placeholder="Ingrese el nombre">
  <span id="errorNombre" class="error"></span>
  <br>
  <label for=""><b>FECHA</b></label>
  <input type="date" name="fecha" id="fecha" class="form-control" value="" required placeholder="Ingrese una fecha">
  <span id="errorFecha" class="error"></span>
  <br>

    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
       <br>
      </div>
      <br>
      <br>

    </div>

  </div>
</form>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>
<script>
function validarFormulario() {
  var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var nombre = document.getElementById("nombre").value;
  var error = false;
  if (!regex.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }


  return !error;
}
</script>
