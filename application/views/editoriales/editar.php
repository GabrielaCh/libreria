<h1><i class="fa-solid fa-city"></i>EDITAR EDITORIAL</h1>
<form class="" method="post" action="<?php echo site_url('editoriales/actualizarEditorial'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
	<input type="hidden" name="id" id="id"
	value="<?php echo $editorialEditar->id; ?>">
  <label for="">
    <b>Nombre:</b>
  </label>
  <input type="text" name="nombre" id="nombre"
	value="<?php echo $editorialEditar->nombre; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <span id="errorNombre" class="error"></span>
  <br>
  <label for="">
    <b>Direccion:</b>
  </label>
  <input type="text" name="direccion" id="direccion"
	value="<?php echo $editorialEditar->direccion; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <span id="errorDireccion" class="error"></span>
  <br>
  <label for="">
    <b>Telefono:</b>
  </label>
  <input type="text" name="telefono" id="telefono"
	value="<?php echo $editorialEditar->telefono; ?>"
  placeholder="Ingrese el numero de telefono..." class="form-control" required>
  <span id="errorTelefono" class="error"></span>
  <br>
  <label for="">
    <b>Correo:</b>
  </label>
  <input type="text" name="correo" id="correo"
	value="<?php echo $editorialEditar->correo; ?>"
  placeholder="Ingrese la editorial..." class="form-control" required>
  <span id="errorCorreo" class="error"></span>
  <br>


    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar </button> &nbsp &nbsp
        <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>


<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>

<script>
function validarFormulario() {
  var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;

  var nombre = document.getElementById("nombre").value;
  var direccion = document.getElementById("direccion").value;


  var error = false;
  if (!regex.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }
  if (!regex.test(direccion)) {
    document.getElementById("errorDireccion").innerHTML = "Por favor, ingrese solo letras en el campo Dirección.";
    error = true;
  } else {
    document.getElementById("errorDireccion").innerHTML = "";
  }


  return !error;
}
</script>

<br>
<br>
