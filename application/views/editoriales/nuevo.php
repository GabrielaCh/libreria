<h1> <i class="fa-solid fa-city"></i> Nueva Editorial</h1>
<br>
<form class="" action="<?php echo site_url('editoriales/guardarEditorial'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
  <label for=""><b>NOMBRE</b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" value="" required placeholder="Ingrese el nombre">
  <span id="errorNombre" class="error"></span>
  <br>
  <label for=""><b>DIRECCION</b></label>
  <input type="text" name="direccion" id="direccion" class="form-control" value="" required placeholder="Ingrese la direccion">
  <span id="errorDireccion" class="error"></span>
  <br>
  <label for=""><b>TELEFONO</b></label>
  <input type="text" name="telefono" id="telefono" class="form-control" value="" required placeholder="Ingrese el telefono de la editorial">
  <span id="errorTelefono" class="error"></span>
  <br>
  <label for=""><b>CORREO</b></label>
  <input type="text" name="correo" id="correo" class="form-control" value="" required placeholder="Ingrese el correo">
  <span id="errorCorreo" class="error"></span>
  <br>

    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
       <br>
      </div>
      <br>
      <br>

    </div>

  </div>
</form>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>
<script>
function validarFormulario() {
  var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var nombre = document.getElementById("nombre").value;
  var direccion = document.getElementById("direccion").value;

  var error = false;
  if (!regex.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }
  if (!regex.test(direccion)) {
    document.getElementById("errorDireccion").innerHTML = "Por favor, ingrese solo letras en el campo Dirección.";
    error = true;
  } else {
    document.getElementById("errorDireccion").innerHTML = "";
  }

  return !error;
}
</script>
