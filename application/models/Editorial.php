<?php
/*CI_Model es la clase padre ya viene definido en Codein */
//Crear el modelo-> Se debe crear en singular
  class Editorial extends CI_Model
  {

    function __construct()
    {
      // Constructor de la clase padre
      parent::__construct();

    }//Fin de la funcion

    //Nueva funcion para insertar nuevo
    function insertar($datos){                              //Primero function-luego cualquier nombre"insertar" -
      //Crear variable
      $respuesta=$this->db->insert("editorial",$datos);      //insert->permite ingresar registros
      return $respuesta;
    }

    //Consulta de trader_cdlrisefall3methods
    //Consulta de datos
    function consultarTodos(){
      //
      $editoriales=$this->db->get("editorial");

      if ($editoriales->num_rows()>0) {

        return $editoriales->result();
      } else {
        return false;
      }
    }//Fin funcion consultarTodos

    //Eliminacion por id
    function eliminar($id){
      $this->db->where("id",$id);
      return $this->db->delete("editorial");
    }

    //Consulta
    function obtenerPorId($id){
      $this->db->where("id",$id);
      $editorial=$this->db->get("editorial");
      if ($editorial->num_rows()>0) {
        return $editorial->row();
      } else {
        return false;
      }
    }

    //Funcion para Actualizar
    function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db->update("editorial",$datos);
    }

  }//Fin de la clase


?>
