<?php
/*CI_Model es la clase padre ya viene definido en Codein */
//Crear el modelo -> Se debe crear en singular
  class Revista extends CI_Model
  {

    function __construct()
    {
      // Constructor de la clase padre
      parent::__construct();

    }//Fin de la funcion

    //Nueva funcion para insertar nuevos
    function insertar($datos){                              //Primero function-luego cualquier nombre"insertar" -
      //Crear variable
      $respuesta=$this->db->insert("revista",$datos);      //insert->permite ingresar registros
      return $respuesta;
    }

    //Consulta de trader_cdlrisefall3methods
    //Consulta de datos
    function consultarTodos(){
      //
      $revistas=$this->db->get("revista");

      if ($revistas->num_rows()>0) {

        return $revistas->result();
      } else {
        return false;
      }
    }//Fin funcion consultarTodos

    //Eliminacion por id
    function eliminar($id){
      $this->db->where("id",$id);
      return $this->db->delete("revista");
    }

    //Consulta de un solo
    function obtenerPorId($id){
      $this->db->where("id",$id);
      $revista=$this->db->get("revista");
      if ($revista->num_rows()>0) {
        return $revista->row();
      } else {
        return false;
      }
    }

    //Funcion para Actualizar
    function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db->update("revista",$datos);
    }


  }//Fin de la clase

?>
