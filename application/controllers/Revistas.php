<?php
//Se crea con el nombre del modelo en plural
//Crear clase Hospitales como controlador
  class Revistas extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Revista");

    }
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoRevistas"]=$this->Revista->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("revistas/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Revista->eliminar($id);
      $this->session->set_flashdata("confirmacion","Revista eliminada exitosamente");
      redirect("revistas/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("revistas/nuevo");
      $this->load->view("footer");
    }
     //echo sirve para mostrar datos en pantalla
    //Capturando datos e insertando
    public function guardarRevista(){

      $datosNuevoRevista=array(
        "nombre"=>$this->input->post("nombre"),
        "fecha"=>$this->input->post("fecha"),
      );
      $this->Revista->insertar($datosNuevoRevista);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Revista guardado exitosamente");

      redirect('revistas/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["revistaEditar"]=$this->Revista->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("revistas/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarRevista(){
      $id=$this->input->post("id");
      $datosRevista=array(
        "nombre"=>$this->input->post("nombre"),
        "fecha"=>$this->input->post("fecha"),
      );
      $this->Revista->actualizar($id,$datosRevista);
      $this->session->set_flashdata("confirmacion",
      "Revista actualizada exitosamente");
      redirect('revistas/index');
    }


  } //Cierre de la clase


?>
