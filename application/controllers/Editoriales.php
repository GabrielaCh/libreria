<?php
//Se crea con el nombre del modelo en plural
//Crear clase Hospitales como controlador
  class Editoriales extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Editorial");

    }
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoEditoriales"]=$this->Editorial->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("editoriales/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Editorial->eliminar($id);
      $this->session->set_flashdata("confirmacion","Editorial eliminada exitosamente");
      redirect("editoriales/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("editoriales/nuevo");
      $this->load->view("footer");
    }
     //echo sirve para mostrar datos en pantalla
    //Capturando datos e insertando
    public function guardarEditorial(){

      $datosNuevoEditorial=array(
        "nombre"=>$this->input->post("nombre"),
        "direccion"=>$this->input->post("direccion"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
      );
      $this->Editorial->insertar($datosNuevoEditorial);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Editorial guardada exitosamente");

      redirect('editoriales/index');
    }


      //Renderizar el formulario de edicion
    public function editar($id){
      $data["editorialEditar"]=$this->Editorial->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("editoriales/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarEditorial(){
      $id=$this->input->post("id");
      $datosEditorial=array(
        "nombre"=>$this->input->post("nombre"),
        "direccion"=>$this->input->post("direccion"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
      );
      $this->Editorial->actualizar($id,$datosEditorial);
      $this->session->set_flashdata("confirmacion",
      "Editorial actualizado exitosamente");
      redirect('editoriales/index');
    }


  } //Cierre de la clase


?>
